import sys
import argparse
import pickle

from datetime import datetime
from sklearn import svm

from data_utils import *


def main(args):
    train_dataset = build_dataset(args.train_dir)
    valid_dataset = build_dataset(args.valid_dir)

    x_train, y_train = build_sklearn_dataloader(train_dataset)
    x_valid, y_valid = build_sklearn_dataloader(valid_dataset)

    clf = svm.SVC()
    print("Training SVM...")
    clf.fit(x_train, y_train)

    val_preds = clf.predict(x_valid)
    val_correct_preds = (val_preds == y_valid).astype(int)
    val_precision = np.sum(val_correct_preds) / val_correct_preds.size

    train_preds = clf.predict(x_train)
    train_correct_preds = (train_preds == y_train).astype(int)
    train_precision = np.sum(train_correct_preds) / train_correct_preds.size

    print(f"Train precision: {train_precision}")
    print(f"Valid precision: {val_precision}")

    # Save trained SVM
    stamp = str(datetime.now()).replace(' ', '_').replace('.', '_').replace(":", "-")
    output_path = Path('./outputs/') / 'svm_model' / stamp
    output_path.mkdir(parents=True)
    filename = str(output_path / 'SVM_model.sav')

    print("Saving model")
    pickle.dump(clf, open(filename, 'wb'))
    print("Model saved")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--train_dir',
        help="Path to train directory",
        type=str,
        default="debug_data/"
    )
    parser.add_argument(
        '--valid_dir',
        help="Path to valid directory",
        type=str,
        default="debug_data/"
    )

    args = parser.parse_args()
    sys.exit(main(args) or 0)
