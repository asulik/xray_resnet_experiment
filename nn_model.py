import torch
from torch.nn import functional as F
from torch import nn
from torch.optim import Adam
from pytorch_lightning.core.lightning import LightningModule


class XrayNN(LightningModule):

    def __init__(self, num_outputs: int, lr):
        super().__init__()

        self.num_outputs = num_outputs
        self.lr = lr

        self.layer1 = nn.Linear(1000, 32)
        self.layer2 = nn.Linear(32, self.num_outputs)

        self.loss = nn.CrossEntropyLoss()

    def forward(self, x):
        batch_size, channels, length = x.size()

        x = x.view(batch_size, -1)
        x = self.layer1(x)
        x = F.relu(x)
        x = self.layer2(x)
        x = F.softmax(x, dim=1)

        return x

    def configure_optimizers(self):
        return Adam(self.parameters(), lr=self.lr)

    def training_step(self, train_batch, batch_idx):
        x, y = train_batch
        logits = self.forward(x)
        loss = self.loss(logits, y)
        return loss

    def validation_step(self, valid_batch, batch_idx):
        x, y = valid_batch
        logits = self.forward(x)
        loss = self.loss(logits, y)
        return loss
