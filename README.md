# Transfer Learning for X-Ray Chest images

Authors: Adam Sulik, Szymon Pabjan

## First run
To run the code you will need to establish virtual environment
for Python 3.6+. To install requirements simply run virtualenv:
```buildoutcfg
source my_venv_directory/bin/activate
```
then use pip to install required dependencies:
```buildoutcfg
pip install -r requirements.txt
```
After that you will be ready to run the code.

## Train neural network solution
The file with trainer for NN is `train_nn.py`. Simply write
```buildoutcfg
python3 train_nn.py -h
```
to check available arguments.
