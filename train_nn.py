import sys
import argparse
from datetime import datetime
from pathlib import Path

from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint

from data_utils import *
from nn_model import XrayNN


def main(args):
    train_dataset = build_dataset(args.train_dir)
    valid_dataset = build_dataset(args.valid_dir)

    train_dataloader = build_dataloader(train_dataset, args.batch_size, num_workers=args.num_workers)
    valid_dataloader = build_dataloader(valid_dataset, args.batch_size, num_workers=args.num_workers)

    stamp = str(datetime.now()).replace(' ', '_').replace('.', '_').replace(":", "-")

    output_path = Path('./outputs/') / 'nn_model' / stamp
    output_path.mkdir(parents=True)

    checkpoint_callback = ModelCheckpoint(
        monitor='val_loss',
        dirpath=output_path,
        filename='nn_train-{epoch:02d}-{val_loss:.2f}',
        save_top_k=3,
        mode='min',
    )

    model = XrayNN(num_outputs=train_dataset.num_classes, lr=args.learning_rate)
    trainer = Trainer(max_epochs=args.num_epochs, callbacks=[checkpoint_callback])
    trainer.fit(model, train_dataloader, valid_dataloader)
    trainer.save_checkpoint(output_path / "best.ckpt")
if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--train_dir',
        type=str,
        help='Directory to train files',
        default='debug_data/'
    )
    parser.add_argument(
        '--valid_dir',
        type=str,
        help='Directory to validation files',
        default='debug_data/',
    )
    parser.add_argument(
        '-b', '--batch_size',
        help='Data loader batch size',
        default=4
    )
    parser.add_argument(
        '-l', '--learning_rate',
        help='Training learning rate',
        type=float,
        default=0.001
    )
    parser.add_argument(
        '--num_epochs',
        help="Number of training epochs",
        type=int,
        default=1,
    )
    parser.add_argument(
        '-w', '--num_workers',
        type=int,
        default=1
    )

    args = parser.parse_args()
    sys.exit(main(args) or 0)
