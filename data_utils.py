import argparse
import sys
import os

import numpy as np
import pandas as pd
import torch as nn

from pathlib import Path
from torchvision import transforms
from PIL import Image
from torch.utils.data import Dataset, DataLoader
from torchvision import models


class XrayDataset(Dataset):
    def __init__(self, meta_dataframe: pd.DataFrame, transform, model):
        self.transform = transform
        self.meta = meta_dataframe
        self.model = model
        self.labels = self.meta.label.unique()
        self.labels.sort()

        self.labels_to_class = {self.labels[i]: i for i in range(len(self.labels))}

    def __len__(self):
        return len(self.meta)

    def __getitem__(self, item):
        img_path = self.meta.iloc[item].path
        label = self.meta.iloc[item].label
        img = Image.open(img_path)
        rgbimg = Image.new("RGB", img.size)
        rgbimg.paste(img)
        img = self.transform(rgbimg)

        with nn.no_grad():
            logits = self.model(img.unsqueeze(0))

        return logits, self.labels_to_class[label]

    @property
    def num_classes(self):
        return len(self.meta.label.unique())

def xray_reader(files_dir: Path) -> pd.DataFrame:
    """
    Reader for dataloader; returns data frame with meta info of dataset.
    Classes should be divided in separate and named folders.

    :rtype: object
    :param files_dir: Path to dataset
    :return: Pandas dataframe with meta info
    """

    df = pd.DataFrame(columns=['path', 'label'])
    for root, dirs, files in os.walk(files_dir):
        for f in files:
            if Path(f).suffix == ".jpeg":
                path_to_file = Path(root) / f
                class_name = Path(root).name.lower()
                df = df.append({'path': str(path_to_file), 'label': class_name}, ignore_index=True)
    return df


def compose_xray():
    preprocess = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])

    return preprocess


def build_dataset(data_path):
    model = models.resnet18(pretrained=True)
    reader = xray_reader(data_path)
    dataset = XrayDataset(reader, transform=compose_xray(), model=model)
    return dataset


def build_dataloader(dataset, batch_size, num_workers):
    return DataLoader(dataset, batch_size, num_workers=num_workers)

def build_sklearn_dataloader(dataset):
    l, _ = dataset[0]
    data_len = l.shape[1]

    X = np.array([])
    Y = np.array([])

    for i in range(len(dataset)):
        logits, c = dataset[i]
        X = np.append(X, logits.numpy().ravel())
        Y = np.append(Y, c)

    X = X.reshape(-1, data_len)

    return X, Y
